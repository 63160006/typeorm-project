import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productRepository = AppDataSource.getRepository(Product)
    
    const products = await productRepository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productRepository.findOneBy({id: 1})
    console.log(updateProduct)
    updateProduct.price = 80
    await productRepository.save(updateProduct)

}).catch(error => console.log(error))
